import junit.framework.TestCase;

public class ExamTest extends TestCase {

	// Test Number: 1
			// Test Objective: To test when we input a value that is below 0 for celcius
			// Test input(s): -1
			// Test expected output(s): 0
			
			public void Dividendtest001() {
				
			assertEquals(-1, Exam.exam1(0));	
			
		// Test Number: 2
						// Test Objective: To test when we input a value that is below 0 for fahrenheit
						// Test input(s): -2
						// Test expected output(s): 0
						
						public void Dividendtest002() {
							
			assertEquals(-2, Exam.exam1(0));	
						
		// Test Number: 3
						// Test Objective: To test when inputs is below the range for fahrenheit
						// Test input(s): 213
						// Test expected output(s): 0
						
						public void Dividendtest003() {
							
							assertEquals(213, Exam.exam1(0));	
						
		// Test Number: 4
						// Test Objective: To test when inputs is below the range for fahrenheit
						// Test input(s): 101
						// Test expected output(s): 0
						
						public void Dividendtest004() {
							
							assertEquals(101, Exam.exam1(0));	
						
		// Test Number: 5
						// Test Objective: To test when all inputs are correct celcius
						// Test input(s): 15
						// Test expected output(s): 59
						
						public void Dividendtest005() {
							
						    assertEquals(15, Exam.exam1(59));	
		// Test Number: 5
						// Test Objective: To test when all inputs are correct fahrenheit
						// Test input(s): 59
						// Test expected output(s): 15
							
						public void Dividendtest006() {
								
							assertEquals(59, Exam.exam1(15));	

}
}